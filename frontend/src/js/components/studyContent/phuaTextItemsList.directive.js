/**
 * Created by svfat on 5/29/16.
 */
angular.module('putonghua')
    .directive('phuaTextItemsList', function(api) {
      return {
        restrict: 'E',
        scope: {},
        template:   '<div class="row">'+
                    '<div class="col-xs-12 content_item text-center">'+
                    '<span class="gray-text" ui-sref="study_content_text_list">Showing {{objects.length}} of {{allObjects.length}} texts. See All</span>'+
                    '</div>'+
                    '</div>'+
                    '<div class="row content_item" ng-repeat="obj in objects">' +
                    '<div class="col-xs-2">' +
                    '<i ui-sref="study_content_text_item({id:obj.id})" class="fa fa-file-text-o fa-2x orange" aria-hidden="true"></i>' +
                    '</div>'+
                    '<div class="col-xs-10">'+
                    '<h5 ui-sref="study_content_text_item({id:obj.id})">' +
                    '{{obj.english_name}}'+
                    '</h5>' +
                    '<h5 class="gray-text">{{obj.level}}</h5>'+
                    '</div>' +
                    '</div>',
        link: function (scope) {
            scope.objects = [];
            api.getStudyContents().then(function (objects) {
                scope.allObjects = objects;
                scope.objects = objects.slice(0,7);
                scope.objects.forEach(function(el) {
                    if(el.study_level == 10) {
                        el.level = 'Beginner'
                    } else if(el.study_level == 20) {
                        el.level = 'Intermediate'
                    } else if (el.study_level == 30) {
                        el.level = 'Advanced'
                    }
                })
            });
        }
      };
    });