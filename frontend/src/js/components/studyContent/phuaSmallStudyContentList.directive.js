/**
 * Created by svfat on 5/29/16.
 */
angular.module('putonghua')
    .directive('phuaSmallStudyContentList', function (api) {
        return {
            restrict: 'E',
            template: '<div ng-repeat="object in titles">' +
                        '<div class="row">' +
                           '<div class="col-xs-9 content-list">' +
                                '<span ng-if="object.type == \'text\'"><i class="fa fa-file-text-o content-list-icon " aria-hidden="true"></i></span>' +
                                '<span ng-if="object.type == \'audio\'"><i class="fa fa-volume-up content-list-icon " aria-hidden="true"></i></span>' +
                                '<span ng-if="object.type == \'video\'"><i class="fa fa-video-camera content-list-icon " aria-hidden="true"></i></span>' +
                                '<a ng-if="object.type == \'text\'" ui-sref="study_content_text_item({id:object.id})">' +
                                    '{$ object.name $}' +
                                '</a>' +
                                '<a ng-if="object.type == \'audio\'" ui-sref="study_content_audio_item({id:object.id})">' +
                                    '{$ object.name $}' +
                                '</a>' +
                                '<a ng-if="object.type == \'video\'" ui-sref="study_content_video_item({id:object.id})">' +
                                    '{$ object.name $}' +
                                '</a>' +                
                            '</div>' +
                            '<div class="col-xs-3">' +
                                '<span class="capitalize">{$ object.type $}</span>' +
                            '</div>' +
                        '</div></div>' +
                    '<strong><a ui-sref="study_content_list">More content...</a></strong>',
            link: function (scope) {
                scope.titles = [];
                api.getRandomTitles().then(function (data) {
                    scope.titles = data;
                });
            }
        };
    });