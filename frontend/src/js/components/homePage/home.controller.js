/**
 * Created by svfat on 4/5/16.
 */
angular.module('putonghua').controller('HomeController', function ($window, $scope, $state, api, AppConfig) {
    document.title = "ChineseVault.com – Free ChineseDictionary, Flashcards, Lessons and more";

    var allMetaElements = document.getElementsByTagName('meta');
    
    for (var i = 0; i < allMetaElements.length; i++) { 
        if (allMetaElements[i].getAttribute("name") == "description") { 
            allMetaElements[i].setAttribute('content', "With a free Chinese / English dictionary, Chinese lessons, flashcards, video and audio study content, as well as text analysis tools, we have everything you need to master the Chinese language."); 
        } else if (allMetaElements[i].getAttribute("name") == "keywords") {
            allMetaElements[i].setAttribute('content', "free chinese dictionary, free chinese lessons, chinese flashcards, chinese study content, chinese tools"); 
        }
    } 



    $scope.query = '';
    $scope.email = '';
    $scope.features = AppConfig.features; // enabled features

    $scope.lastSearches = [];
    $scope.randomCharacters = [];

    api.getLatestSearches().then(function (searches) {
        $scope.lastSearches = searches;
    });

    api.getRandomWords().then(function (words) {
        $scope.randomCharacters = words;
    });


    $scope.submitQuery = function () {
        $state.go('dictionary', {'q': $scope.query});
    };

    $scope.submitSignup = function () {
        $state.go('signup', {'email': $scope.email});
    };
})