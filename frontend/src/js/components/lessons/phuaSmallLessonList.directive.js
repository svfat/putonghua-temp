/**
 * Created by svfat on 5/29/16.
 */
angular.module('putonghua')
    .directive('phuaSmallLessonList', function (api) {
        return {
            restrict: 'E',
            template: '<strong>Latest:</strong>' +
            '<div ng-repeat="lesson in latestLessons">' +
            '<div class="row">' +
            '<div class="col-xs-9">' +
            '<a ui-sref="lesson_detail({lessonNumber: lesson.number})">' +
            'Lesson {$ lesson.number $}: {$ lesson.name $}' +
            '</a>' +
            '</div>' +
            '<div class="col-xs-3">' +
            '<span>{$ lesson.new_words.length $}&nbsp;Words</span>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<strong>Popular:</strong>' +
            '<div ng-repeat="lesson in popularLessons">' +
            '<div class="row">' +
            '<div class="col-xs-9">' +
            '<a ui-sref="lesson_detail({lessonNumber: lesson.number})">' +
            'Lesson {$ lesson.number $}: {$ lesson.name $}' +
            '</a>' +
            '</div>' +
            '<div class="col-xs-3">' +
            '<span>{$ lesson.new_words.length $}&nbsp;Words</span>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<strong><a ui-sref="lesson">More lessons...</a></strong>',
            link: function (scope) {
                scope.latestLessons = [];
                scope.popularLessons = [];
                api.getLessons().then(function (lessons) {
                    scope.latestLessons = lessons.reverse().slice(0, 3);
                });
                api.getPopularLessons().then(function (lessons) {
                    scope.popularLessons = lessons.objects.slice(0, 3);
                });
            }
        };
    });