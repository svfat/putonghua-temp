angular.module('putonghua')
 .controller(
        'TextToolController',
        function ($window, $scope, $rootScope, $http, $location, ngDialog, WordList, searchcacheService, PhuaAudio) {

            document.title = "Chinese Text Reader - ChineseVault.com";

            var allMetaElements = document.getElementsByTagName('meta');
            
            for (var i = 0; i < allMetaElements.length; i++) { 
                if (allMetaElements[i].getAttribute("name") == "description") { 
                    allMetaElements[i].setAttribute('content', "Learn to read and understand Chinese with our Text Tool. Click on any Chinese word for a full definition, instantly add Pinyin to Chinese text, or convert from simplified to traditional characters."); 
                } else if (allMetaElements[i].getAttribute("name") == "keywords") {
                    allMetaElements[i].setAttribute('content', "analyze chinese, add pinyin, convert pinyin, simplified to traditional, chinese text tools"); 
                }
            }

             var blankResult = function () {
                return {
                     'original':'',
                     'pinyin': '',
                     'pin1yin1': '',
                     'words': [],
                     'origtext' : {
                                    selected: null,
                                    words: []
                                  },
                     'analyzeFinished': false
                   };
                };
            $scope.savedResult = blankResult();
            $scope.resetTextTool = function () {
                $scope.result = blankResult();
                $scope.controlPanel = {
                                        'language': 'chinese',
                                        'tradorsimpl': 'simplified',
                                        'analyze': 'on',
                                        'pinyin': 'off',
                                        'select' : {
                                             sortby: '2',
                                             show: '0'
                                           }
                                      };
            };
            $scope.resetTextToolSetLang = function(language) {
                $scope.resetTextTool();
                $scope.controlPanel.language = language;
            };


            $scope.submitText = function () {
                analyzeText($scope.result.original, $scope.controlPanel).then(function(r) {
                    $scope.result = r;
                }
                );
            };
            $scope.saveAndLoadState = function(language) {
              var tmp = $scope.result;
              $scope.resetTextToolSetLang(language);
              $scope.result = $scope.savedResult;
              $scope.savedResult = tmp;
            };

            $scope.newAnalysis = function () {
              $scope.resetTextTool();
              $scope.savedResult = blankResult();
            };

            $scope.resetTextTool();

            var analyzeText = function (text, controlPanel) {
                var result = blankResult();
                result.original = text;

                if (controlPanel.language == 'pinyin') {
                    return $http({
                                        method: 'POST',
                                        url: '/api/1.0/texttool/pinyin2p1y1/',
                                        data: {'text': text},
                                        headers: {
                                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                                        }
                                    })
                .then(function(r) {
                            if (r.data.success) {
                                result.pin1yin1 = r.data.result;
                            }
                            return $http({
                                        method: 'POST',
                                        url: '/api/1.0/texttool/p1y12pinyin/',
                                        data: {'text': text},
                                        headers: {
                                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                                        }
                                    });
                        })
                .then(function(r) {
                        if (r.data.success) {
                            result.pinyin = r.data.result;
                        }
                        result.words = ['E','R','R','O','R'];
                        result.analyzeFinished = true;
                        return result;
                    });}
                else if (controlPanel.language == 'chinese') {
                    return $http({
                                        method: 'POST',
                                        url: '/api/1.0/texttool/',
                                        data: {'text': text},
                                        headers: {
                                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                                        }
                                    })
                    .then(function (r) {
                            if (r.data.success) {
                                var arr = r.data.words;
                                result.origtext.words = r.data.words;
                                var add_word = function(word) {
                                    var word_arr = result.words;
                                    word_arr.forEach(function(item, i, word_arr) {
                                            if (item.id == word.id) {
                                              word_arr[i].occurence +=1;
                                              word.occurence = word_arr[i].occurence;
                                            }
                                        }
                                    );
                                    result.words.push(word);
                                    return word;
                                };
                                var english = /^[A-Za-z0-9\s\r\n]*$/;
                                arr.forEach(function(item, i, arr) {
                                  if (!(english.test(item))) {
                                      var promise = searchcacheService.getData(item);
                                      promise.then(function(r) {  // this is only run after $http completes
                                           if (r) {
                                                    var word = r;
                                                    word.occurence = 1;
                                                    word.position = i;
                                                    word = add_word(word);
                                                    result.origtext.words[i] = word;
                                                }
                                        });
                                    }
                                  });
                            } else {
                                result.words = [];
                            }
                        })
                    .then(function() {
                        result.analyzeFinished = true;
                        return result;
                    });
                }
            };

        /////////// SOUND /////////////
        $scope.playSound = PhuaAudio.playSound;

        // TODO: refactor
        $scope.soundIsPlaying = false;

        $scope.$on('word-audio.play', function (event, data) {
          $scope.soundIsPlaying = true;
          $scope.$apply();
        });
        $scope.$on('word-audio.ended', function (event, data) {
          $scope.soundIsPlaying = false;
          $scope.$apply();
        });
        ////////////////////////////////


        $scope.selectWord = function selectWord(word) {
            $scope.infoword = word;
            $rootScope.globals.SelectedWord = word;
            $scope.result.origtext.selected = word;
        };


        $scope.openAddToListDialog = WordList.openAddToListDialog;

        $scope.playSound = PhuaAudio.playSound;


        }
    )
;
