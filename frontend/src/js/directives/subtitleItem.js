angular.module('putonghua')
    .directive('subtitleItem', ['$q', '$sce', 'api', '$rootScope', 'MetaTags', function($q, $sce, api, $rootScope, MetaTags) {
      return {
        restrict: 'E',
        templateUrl: '/static/templates/studyContent/subtitleItem.html',
        transclude: true,
        scope: {
          id: "=id",
          contentType: "=contentType"
        },
        link: function(scope) {
              var videogularAPI = null;

              var itemsInRow = 30;

              scope.onPlayerReady = function(API) {
                videogularAPI = API;
                videogularAPI.setVolume(1);
              };

              // $rootScope.MetaTags.title = {Content Name} - Chinese Audio Content – ChineseVault.com

              var cnvTime = function(s) {
                /*
                  Convert given time from '00:00:00,000' format to milliseconds
                */
                var tmp1 = s.split(",");
                var ms = parseInt(tmp1[1]);
                var tmp2 = tmp1[0].split(':');
                var hours = parseInt(tmp2[0]);
                var minutes = parseInt(tmp2[1]);
                var seconds = parseInt(tmp2[2]);
                var result = ms + seconds*1000 + minutes*60*1000 + hours*60*60*1000;
                return result;
              };


              scope.onUpdateTime = function(currentTime, totalTime) {
                  scope.currentTime = currentTime;
                  scope.totalTime = totalTime;
                  var arr = scope.item.srt;
                  var t = currentTime*1000;
                  for (var i = 0; i < arr.length; i++) {
                    var row = arr[i];
                    for (var j=0; j < row.length; j++) {
                      if (t >= cnvTime(row[j].startTime) && t <= cnvTime(row[j].endTime)) {
                         scope.currentText = row[j].text;
                         scope.currentTextId = row[j].id;
                      }
                    }
                   }

              };

              scope.selectedWord = null;

              scope.searchWord = function(query) {
                api.searchChineseWord(query).then(function(word) {
                  $rootScope.globals.SelectedWord = word;
                  scope.selectedWord = word;
                });
              };

              scope.currentText = "None";
              scope.currentTextId = null;

              scope.$watch('currentTextId', function(newValue, oldValue) {
                scope.$broadcast('srt-update', newValue);
              });

              scope.item = {
                name: "",
                filename: "",
                srt: {}
              };
              scope.mediaconfig = {
                sources: [
                ],
                theme: {
                  url: "/static/css/videogular.css"
                },
                playbackSpeeds: ['0.7', '0.8',  '0.9', '1', '1.1', '1.2', '1.3']
              };

              var subtitled;
              var t = scope.contentType;
              if (scope.contentType == 'audio') {
                subtitled = api.getAudioStudyItem(scope.id);
              }
              else if (scope.contentType == 'video') {
                subtitled = api.getVideoStudyItem(scope.id);
              }
              subtitled.then(function (item) {
                scope.item.name = item.name;


                if (scope.contentType == 'audio') {
                  document.title = scope.item.name + " - Chinese Audio Content – ChineseVault.com";

                  var allMetaElements = document.getElementsByTagName('meta');
                  
                  for (var i = 0; i < allMetaElements.length; i++) { 
                      if (allMetaElements[i].getAttribute("name") == "description") { 
                          allMetaElements[i].setAttribute('content', item.description); 
                      } else if (allMetaElements[i].getAttribute("name") == "keywords") {
                          allMetaElements[i].setAttribute('content', "chinese audio study content, learn chinese with audio, learn chinese free, chinese audio"); 
                      }
                  }
                }
                else if (scope.contentType == 'video') {
                  document.title = scope.item.name + " - Chinese Video Content – ChineseVault.com";

                  var allMetaElements = document.getElementsByTagName('meta');
                  
                  for (var i = 0; i < allMetaElements.length; i++) { 
                      if (allMetaElements[i].getAttribute("name") == "description") { 
                          allMetaElements[i].setAttribute('content', item.description); 
                      } else if (allMetaElements[i].getAttribute("name") == "keywords") {
                          allMetaElements[i].setAttribute('content', "chinese video study content, learn chinese with videos, learn chinese free, chinese videos"); 
                      }
                  }
                }


                var fn = item.filename;
                var extension = item.filename.substr(item.filename.length - 3);
                var mediaType = null;
                if (extension == 'mp3') {
                  scope.item.type = 'audio';
                  mediaType = 'audio/mpeg';
                }
                else if (extension == 'mp4') {
                  scope.item.type = 'video';
                  mediaType = 'video/mp4';
                }
                else {
                  throw new Error("Media type error!");
                }
                var sources = [{
                                src: $sce.trustAsResourceUrl(fn), type: mediaType
                            }];
                var that = this;
                var data = parser.fromSrt(item.srt, false);
                scope.item.srt = [];

                var convertedWords = [];
                var promise_array = [];

                function callbackCreator(id, startTime, endTime) {
                  return function(result) {
                    var words = result.words;
                    var resultArr = [];
                    convertedWords.push({text:words, id:parseInt(id), startTime:startTime, endTime:endTime})
                  };
                }
                for (var i = 0; i < data.length; i++) {
                  var callback = callbackCreator(data[i].id, data[i].startTime, data[i].endTime);
                  promise_array.push(api.textToolSplitChinese(data[i].text).then(callback));
                }

                function rowCharsLength(row) {
                  // total count of characters in row
                  var result = 0;
                  for (var i = 0; i < row.length; i++) {
                    result = result + row[i].text.length;
                  }
                  return result;
                }


                $q.all(promise_array)
                  .then(
                      function() {
                          // after fetching all data from API, convert in to rows ~30 chars length
                          var row = [],
                              tmp = [];

                          // sort
                          convertedWords.sort(function(a, b){
                              var keyA = a.id,
                                  keyB = b.id;
                              if(keyA < keyB) return -1;
                              if(keyA > keyB) return 1;
                              return 0;
                          });
                          // make flat

                          for (var i = 0; i < convertedWords.length; i++) {
                            var r = convertedWords[i];
                            for (var j = 0; j < r.text.length; j++) {
                              tmp.push({text:r.text[j], id:parseInt(r.id), startTime:r.startTime, endTime:r.endTime});
                            }
                          }
                          convertedWords = tmp;
                          for (var i = 0; i < convertedWords.length; i++) {
                              row.push(convertedWords[i]);
                              if (rowCharsLength(row) >= itemsInRow) {
                                // start new row if previous is full
                                scope.item.srt.push(row);
                                row = [];
                              }
                            }
                          if (row.length > 0) { // if something is left, append it to array
                            scope.item.srt.push(row);
                          }
                          console.log(scope.item.srt)
                      }
                  );

                scope.mediaconfig.sources = sources;
              });
            }

      };
    }]);
angular.module('putonghua')
.directive('scrollOnUpdate', function() {
  function link(scope, element, attrs) {
    var srtContainer = element.parent().parent(),
        row = element.parent();
        var magicValue = 20;
    scope.$on('srt-update', function(event, mass) {
      if (scope.wordId == mass) {
        var scrollTo = srtContainer.scrollTop() +  row.position().top - magicValue;
        srtContainer.animate({scrollTop: scrollTo}, "slow");
      }
    });
  }

  return {
    scope: {
      wordId: '=scrollOnUpdate'
    },
    link: link
  };
});
