angular.module('putonghua')
    .directive('subtitleItemList', function(api, $state) {
      return {
        restrict: 'E',
        templateUrl: '/static/templates/studyContent/subtitleItemList.html',
        scope: {
          contentType: '@'
        },
        link: function (scope, element, attrs) {
            if (scope.contentType == 'audio') {
                api.getAudioStudyItems().then(function (items) {
                    scope.items = items;
                });

                document.title = "Chinese Audio Study Content - ChineseVault.com";

                var allMetaElements = document.getElementsByTagName('meta');
                
                for (var i = 0; i < allMetaElements.length; i++) { 
                    if (allMetaElements[i].getAttribute("name") == "description") { 
                        allMetaElements[i].setAttribute('content', "Learn Chinese with real audio study content. Improve your listening and reading skills by listening to real Chinese audio, with a complete transcript to help you learn."); 
                    } else if (allMetaElements[i].getAttribute("name") == "keywords") {
                        allMetaElements[i].setAttribute('content', "chinese audio study content, learn chinese with audio, learn chinese free, chinese audio"); 
                    }
                }
            }
            else if (scope.contentType == 'video') {
                api.getVideoStudyItems().then(function (items) {
                    scope.items = items;
                });

                document.title = "Chinese Video Study Content - ChineseVault.com";

                var allMetaElements = document.getElementsByTagName('meta');
                
                for (var i = 0; i < allMetaElements.length; i++) { 
                    if (allMetaElements[i].getAttribute("name") == "description") { 
                        allMetaElements[i].setAttribute('content', "Learn Chinese with real video study content. Improve your listening and reading skills by watching real Chinese videos, with a complete transcript to help you learn."); 
                    } else if (allMetaElements[i].getAttribute("name") == "keywords") {
                        allMetaElements[i].setAttribute('content', "chinese video study content, learn chinese with videos, learn chinese free, chinese videos"); 
                    }
                }
            }
            else {
                throw new Error('Item type is not declared in directive <subtitle-item-list>');
            }
            scope.go = function (state, params) {
               $state.go(state, params);
        };
        }
      };
    });
