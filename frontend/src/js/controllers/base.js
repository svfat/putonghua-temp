angular.module('putonghua')
    .controller(
        'BaseController',
        function ($state, $scope, $rootScope, $location, ngDialog, Auth, WordInfoEvents, AuthEvents, AppConfig) {
            $rootScope.globals = {
                SelectedWord: ''
            };

            $scope.makeActive = function (state) {
                $scope.currentState = state;
                console.log(state);
            }
        
            $rootScope.$watch('globals.SelectedWord', function (value) {
                var wrapper = angular.element('#wrapper');
                $scope.SelectedWord = $rootScope.globals.SelectedWord;
                if (value == '') {
                    wrapper.removeClass('toggle');
                }
                if (value !== '') {
                    wrapper.addClass('toggle');
                }

            });
            $rootScope.$on(WordInfoEvents.WORDINFO_CLOSE, function () {
                $rootScope.globals.SelectedWord = ''
            });
            $scope.login = function (SignIn) {
                if (SignIn === true) {
                    $rootScope.$broadcast(AuthEvents.NOT_AUTHENTICATED_SIGNIN);
                }
                else {
                    $rootScope.$broadcast(AuthEvents.NOT_AUTHENTICATED);
                }
            };
            $scope.features = AppConfig.features; // features enabled
        }
    )
;
