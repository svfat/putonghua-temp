angular.module('putonghua')
    .controller('ContactController', function ($scope, $location, $http) {
        $scope.form = {
            name: '',
            email: '',
            subject: '',
            message: '',
            errors: {}
        };

        $scope.contactSuccess = false;

        $scope.submitForm = function () {
            $http({
                method: 'POST',
                url: '/contact/',
                data: $scope.form,
                transformRequest: function (data) {
                    return $.param(data);
                }
            }).success(function (response) {
                if (response.success) {
                    $scope.contactSuccess = true;
                } else {
                    $scope.form.errors = response.errors;
                }
            });
        };
    });
