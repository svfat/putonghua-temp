from putonghua.settings.base import *
import raven

ADMINS = (
    ('Robert Tonkin', 'robertjtonkin@hotmail.com'),
    ('Stanislav Fateev', 'fateevstas+phua@yandex.ru'),
)

EMAIL_BACKEND = 'django_mailgun.MailgunBackend'
MAILGUN_ACCESS_KEY = 'key-67f0d2fdd6775028a4b7808f23a4bd1b'
MAILGUN_SERVER_NAME = 'chinesevault.com'





USERENA_ACTIVATION_REQUIRED = True
STATIC_ROOT = os.path.abspath(os.path.join(BASE_DIR, '../staticfiles/'))
STATICFILES_DIRS = ()

VOICERSS_API_KEY = '45e9302eefad43f3b918493d1c0ac66a'

import raven

RAVEN_CONFIG = {
    'dsn': 'http://4e1b0313b3c342a29595ade5139f0cd2:30ffbd82a4fe4cc0b85dfc11177182ee@sentry.putonghua.social/2',
    # If you are using git, you can also automatically configure the
    # release based on the git info.
    'release': raven.fetch_git_sha(os.path.dirname('/home/phua/putonghua-social/')),
}


TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        # 'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'DIRS': [os.path.abspath(os.path.join(BASE_DIR, '../staticfiles/'))],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]


DEBUG = True
ALLOWED_HOSTS = ['putonghua.social', 'chinesevault.com']

INSTALLED_APPS += (
    'raven.contrib.django',
    )

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'root': {
        'level': 'WARNING',
        'handlers': ['sentry'],
    },
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s '
                      '%(process)d %(thread)d %(message)s'
        },
    },
    'handlers': {
        'sentry': {
            'level': 'ERROR',
            'class': 'raven.contrib.django.raven_compat.handlers.SentryHandler',
            'tags': {'custom-tag': 'x'},
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose'
        }
    },
    'loggers': {
        'django.db.backends': {
            'level': 'ERROR',
            'handlers': ['console'],
            'propagate': False,
        },
        'raven': {
            'level': 'DEBUG',
            'handlers': ['console'],
            'propagate': False,
        },
        'sentry.errors': {
            'level': 'DEBUG',
            'handlers': ['console'],
            'propagate': False,
        },
    },
}

MIDDLEWARE_CLASSES = (
    'raven.contrib.django.raven_compat.middleware.Sentry404CatchMiddleware',
) + MIDDLEWARE_CLASSES


# STRIPE_PUBLIC_KEY = os.environ.get("STRIPE_PUBLIC_KEY", "pk_live_jePyfu0lZshJHQPUhwxvYLEG")
# STRIPE_SECRET_KEY = os.environ.get("STRIPE_SECRET_KEY", "sk_live_4ItdAXbtpjCqfRY6tg4Ka93p")
